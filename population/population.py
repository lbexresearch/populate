#!/usr/bin/env python3
"""Generate a nd modify Populations"""
# coding=utf-8
import random
import math
import logging
import argparse
from argparse import RawTextHelpFormatter
from copy import deepcopy
from calendar import monthrange
from datetime import datetime
from dateutil.relativedelta import relativedelta
from person.person import Person
from country.country import Country

class Demography():
    """
    Demography encompasses the study of the size, structure, and distribution of
    these populations, and spatial or temporal changes in them in response to
    birth, migration, ageing, and death. Based on the demographic research of
    the earth, earth's population up to the year 2050 and 2100 can be estimated
    by demographers. Demographics are quantifiable characteristics of a given
    population.

    See : http://www.columbia.edu/itc/hs/pubhealth/modules/demography/populationRates.html
    """

    # Age ranges
    age_ranges = [(130, 150),
                  (120, 130),
                  (110, 120),
                  (100, 110),
                  (95, 100),
                  (90, 95),
                  (85, 90),
                  (80, 85),
                  (75, 80),
                  (70, 75),
                  (65, 70),
                  (60, 65),
                  (55, 60),
                  (50, 55),
                  (45, 50),
                  (40, 45),
                  (35, 40),
                  (30, 35),
                  (25, 30),
                  (20, 25),
                  (10, 15),
                  (5, 10),
                  (1, 5),
                  (0, 1)]

    # tuple ( <over age>, <under age> ) description.
    age_range_description = []
    age_range_description = {(0, 1):     "OO years",
                             (1, 5):     "1 to 4 years",
                             (5, 10):    "5 to 9 years",
                             (10, 15):   "10 to 14 years",
                             (20, 25):   "15 to 19 years",
                             (25, 30):   "20 to 24 years",
                             (30, 35):   "30 to 34 years",
                             (35, 40):   "35 to 39 years",
                             (40, 45):   "40 to 44 years",
                             (45, 50):   "45 to 49 years",
                             (50, 55):   "50 to 54 years",
                             (55, 60):   "55 to 59 years",
                             (60, 65):   "60 to 64 years",
                             (65, 70):   "65 to 69 years",
                             (70, 75):   "70 to 74 years",
                             (75, 80):   "75 to 79 years",
                             (80, 85):   "80 to 84 years",
                             (85, 90):   "85 to 89 years",
                             (90, 95):   "90 to 94 years",
                             (95, 100):  "95 to 99 years",
                             (100, 110): "100 to 109 years",
                             (110, 120): "110 to 119 years",
                             (120, 130): "120 to 129 years",
                             (130, 150): "130 to 150 years"}




    crude_birth_rate = 12.21           # birth/thousand
    crude_death_rate = 9.067           # deaths/thousand
    crude_net_migration = 3.678        # people/thousand
    male_life_expectation = 81.02      # years
    female_life_expectation = 84.302   # years
    total_fertility_rate = 1.926       # children/woman
    net_reproduction_rate = 0.929      # surviving daughters/woman
    sex_ratio_at_birth = 1.06          # males per female
    infant_mortality_rate = 2.501      # deaths/1,000 live births
    under_five_mortality = 2.927       # deaths/thousand
    mean_age_at_childbearing = 30.896  # years
    rate_of_natural_increase = 3.176
    death_before_40_probability_total = 1.401  # %
    death_before_40_probability_male = 1.8     # %
    death_before_40_probability_female = 0.982 # %
    death_before_60_probability_total = 5.379  # %
    death_before_60_probability_male = 6.347   # %
    death_before_60_probability_female = 4.368 # %
    crude_suicide_rate = 13.2                  # suicides/thousand
    median_age_total = 41.2                    # years
    median_age_female = 42.2                   # years
    median_age_male = 40.2                     # years

    death_rate = {}
    death_rate['male'] = {}
    death_rate['female'] = {}

    death_rate['male'][(0, 1)] = 5
    death_rate['male'][(1, 5)] = 0.5
    death_rate['male'][(5, 10)] = 0.5
    death_rate['male'][(10, 15)] = 0.5
    death_rate['male'][(15, 20)] = 0.5
    death_rate['male'][(20, 25)] = 1.5
    death_rate['male'][(25, 30)] = 1.5
    death_rate['male'][(30, 35)] = 2.5
    death_rate['male'][(35, 40)] = 2.5
    death_rate['male'][(40, 45)] = 2.5
    death_rate['male'][(45, 50)] = 5.5
    death_rate['male'][(50, 55)] = 7.5
    death_rate['male'][(55, 60)] = 10.5
    death_rate['male'][(60, 65)] = 29.5
    death_rate['male'][(65, 70)] = 31.5
    death_rate['male'][(70, 75)] = 32.5
    death_rate['male'][(75, 80)] = 35.5
    death_rate['male'][(80, 85)] = 40.5
    death_rate['male'][(85, 90)] = 50.5
    death_rate['male'][(90, 95)] = 60.5
    death_rate['male'][(95, 100)] = 60.5
    death_rate['male'][(100, 110)] = 70.5
    death_rate['male'][(110, 120)] = 80.5
    death_rate['male'][(120, 130)] = 95.0
    death_rate['male'][(130, 150)] = 100

    death_rate['female'][(0, 1)] = 5
    death_rate['female'][(1, 5)] = 0.5
    death_rate['female'][(5, 10)] = 0.5
    death_rate['female'][(10, 15)] = 0.5
    death_rate['female'][(15, 20)] = 0.5
    death_rate['female'][(20, 25)] = 1.5
    death_rate['female'][(25, 30)] = 1.5
    death_rate['female'][(30, 35)] = 1.5
    death_rate['female'][(35, 40)] = 2.5
    death_rate['female'][(40, 45)] = 2.5
    death_rate['female'][(45, 50)] = 5.5
    death_rate['female'][(50, 55)] = 7.5
    death_rate['female'][(55, 60)] = 10.5
    death_rate['female'][(60, 65)] = 29.5
    death_rate['female'][(65, 70)] = 31.5
    death_rate['female'][(70, 75)] = 32.5
    death_rate['female'][(75, 80)] = 35.5
    death_rate['female'][(80, 85)] = 40.5
    death_rate['female'][(85, 90)] = 50.5
    death_rate['female'][(90, 95)] = 50.5
    death_rate['female'][(95, 100)] = 60.5
    death_rate['female'][(100, 110)] = 70.5
    death_rate['female'][(110, 120)] = 80.5
    death_rate['female'][(120, 130)] = 95.0
    death_rate['female'][(130, 150)] = 100

    age_specific_fertility_rate = {}
    age_specific_fertility_rate['female'] = {}


    age_specific_fertility_rate['female'][(0, 1)] = 0
    age_specific_fertility_rate['female'][(1, 5)] = 0
    age_specific_fertility_rate['female'][(5, 10)] = 0
    age_specific_fertility_rate['female'][(10, 15)] = 0
    age_specific_fertility_rate['female'][(15, 20)] = 0.025
    age_specific_fertility_rate['female'][(20, 25)] = 0.250
    age_specific_fertility_rate['female'][(25, 30)] = 0.113
    age_specific_fertility_rate['female'][(30, 35)] = 0.132
    age_specific_fertility_rate['female'][(35, 40)] = 0.097
    age_specific_fertility_rate['female'][(40, 45)] = 0.050
    age_specific_fertility_rate['female'][(45, 50)] = 0.05
    age_specific_fertility_rate['female'][(50, 55)] = 0.0
    age_specific_fertility_rate['female'][(55, 60)] = 0.0
    age_specific_fertility_rate['female'][(60, 65)] = 0.0
    age_specific_fertility_rate['female'][(65, 70)] = 0.0
    age_specific_fertility_rate['female'][(70, 75)] = 0.0
    age_specific_fertility_rate['female'][(75, 80)] = 0.0
    age_specific_fertility_rate['female'][(80, 85)] = 0.0
    age_specific_fertility_rate['female'][(85, 90)] = 0.0
    age_specific_fertility_rate['female'][(90, 95)] = 0.0
    age_specific_fertility_rate['female'][(95, 100)] = 0.0
    age_specific_fertility_rate['female'][(100, 110)] = 0.0
    age_specific_fertility_rate['female'][(110, 120)] = 0.0
    age_specific_fertility_rate['female'][(120, 130)] = 0.0
    age_specific_fertility_rate['female'][(130, 150)] = 0.0


    def __init__(self, crude_birth_rate=11.0, crude_death_rate=9.2):

        # of births per 1,000 persons in a population over a given period of time (i.e. 1 year).
        self.crude_birth_rate = crude_birth_rate
        # self.general_fertility_rate      =
        # self.age_specific_fertility_rate =
        self.total_fertility_rate = 11.8
        self.gross_fertility_rate = 1.85
        # self.net_reproduction_rate       =
        # # of deaths per 1,000 persons in a population over a given period of time (i.e. 1 year).
        self.crude_death_rate = crude_death_rate

        self.infant_mortality_rate = 2.6
        self.life_expectacy = 82.1

        # I is the number of immigrants or in-migrants
        # E is the number of emigrants or out-migrants
        # P is the total midyear population of the country or designated area.
        # Crude net migration rate = I – E / P * 1,000
        self.crude_net_migration_rate = 1.62
        self.net_migration_rate = 5.4

    def __str__(self):
        return "crude_birth_rate         : " + str(self.crude_birth_rate) + "\n" + \
               "total_fertility_rate     : " + str(self.total_fertility_rate) + "\n" + \
               "gross_fertility_rate     : " + str(self.gross_fertility_rate) + "\n" + \
               "crude_death_rate         : " + str(self.crude_death_rate) + "\n" + \
               "infant_mortality_rate    : " + str(self.infant_mortality_rate) + "\n" + \
               "life_expectacy           : " + str(self.life_expectacy) + "\n" + \
               "crude_net_migration_rate : " + str(self.crude_net_migration_rate) + "\n" + \
               "net_migration_rate       : " + str(self.net_migration_rate)


    def set_crude_birth_rate(self, cbr=None):
        """Calculate crude birth rate"""
        if cbr:
            self.crude_birth_rate = cbr


class Couples():
    """
    A list of fertil couples to pick from when adding birth.
    """
    def __init__(self):
        self.couples = []

    def add(self, person1=None, person2=None):
        """Add a couple"""
        self.couples.append((person1, person2))

    def delete(self, person=None):
        """Delete a couple, specify one person in the couple"""
        for couple in self.couples:
            if person in couple:
                self.couples.remove(couple)


class Population():
    """
    The population is the people living in a specific area at a specific time.
    In this class it's just two lists of unique numbers one for males and one
    for females.

    The initial population at begin is distributed over one generation from
    begin time. When you start advancing the time the birth rate will decide
    when to add people. You can also add people manually.

    Population numbers for each mid year and year.
    """
    def __init__(self, begin=1900, population=10000, \
                 crude_birth_rate=15, crude_death_rate=1.1, generation=30, country='sv'):

        self.time = datetime(year=int(begin), month=1, day=1)
        self.males = []          # List of male individuals
        self.females = []          # List of female individuals
        self.generation = generation  # From birth to have own children, normally 30 years
        self.country = country
        self.age_range_index = {}

        self.demographics = Demography(crude_birth_rate=crude_birth_rate,\
                                       crude_death_rate=crude_death_rate)
        self.couples = Couples()

        # Calculate chunks and births / month
        # total_births = population * self.demographics.crude_birth_rate /1000
        # births_per_month = total_births % 12
        births_per_month = 2
        # birth_rest = total_births // 12

        for year in range(generation):
            logging.debug("Generation : %s  Time : %s", year, self.time)
            # Calculate chunks and births / month
            # total_births = population * self.cbr
            # births_per_month = total_births % 12
            births_per_month = 27
            # births_per_day = 1
            # births_per_hour = 1
            # birth_rest = total_births // 12
            # population = population + total_births

            month = 0
            for month in range(1, 12):
                day = 0
                for _ in range(births_per_month):
                    day = day + 1
                    new_person = Person(year=str(begin + year), month=str(month),\
                                        day=str(day), country=country)
                    person = deepcopy(new_person)
                    if person.is_male():
                        self.males.append(person)
                    else:
                        self.females.append(person)
            self.time = datetime(begin+year, month, day)
        self.age_range_index['male'] = {}
        self.age_range_index['female'] = {}
        # Initiate age ranges, define keys
        for age_range in self.demographics.age_ranges:
            self.age_range_index['male'][age_range] = (0, 0)
            self.age_range_index['female'][age_range] = (0, 0)

        self.update_age_range_index()


    def __str__(self):
        return "\nTime                        : " + self.time.strftime("%Y-%m-%d") +\
               "\nSize of population          : " + str(self.size()) + \
               "\nSize of male population     : " + str(len(self.males)) + \
               "\nSize of female population   : " + str(len(self.females)) + \
               "\n\nDemograpics\n===========\n" + str(self.demographics)


    def get_birth_per_month(self):
        """
        Return the number of expected births in a month using the birthrate and
        the size of the population.

        Crude Birth Rate is specified in birth per 1000 inhabitants.

        """
        size = self.size()
        crude_birth_rate = self.demographics.crude_birth_rate
        births_per_month = int(size * crude_birth_rate) / 12000
        logging.debug("BIRTHS PER MONTH = size %s *  %s = %s", \
                      size, crude_birth_rate, births_per_month)
        return int(births_per_month)


    def distribute_month(self, length=None, number=0):
        """
        Randomize a number over a list that represent the days of a month. Then
        the number is births over that month. Birtsh are randomized over the days.
        """
        month_list = []
        for _ in range(length):
            month_list.append(0)

        for _ in range(number):
            i = random.randint(0, length-1)
            month_list[i] += 1

        return month_list



    def add_birth(self, months=1):
        """
        Use the demographic data to add new persons to the population.
        To get a better distribution over the month the births are randomized
        over the day of the month.

        This method also set the time end of the last month.
        Pick Parents in the couple list.
        """
        month = 1
        births_per_month = self.get_birth_per_month()
        births_per_day = births_per_month/28
        logging.debug("ADD BIRTH : births_per_month = %s  Births_per_day : %s",\
                      births_per_month, births_per_day)
        logging.debug("YEAR : %s", self.time.year)
        self.update_age_range_index()
        _, father_last_index = self.age_range_index['male'][(20, 25)]
        father_first_index, _ = self.age_range_index['male'][(35, 40)]
        _, mother_last_index = self.age_range_index['female'][(20, 25)]
        mother_first_index, _ = self.age_range_index['female'][(35, 40)]

        print("Fathers : ", father_first_index, father_last_index)
        if father_last_index < father_first_index:
            father_last_index = father_first_index + 10
        print("Mothers : ", mother_first_index, mother_last_index)
        if mother_last_index < mother_first_index:
            mother_last_index = mother_first_index + 10

        for month in range(1, months):
            day_number = 0
            (_, days_in_month) = monthrange(self.time.year, month)
            days = self.distribute_month(length=days_in_month, number=births_per_month)

            for day in days:
                day_number += 1
                ### logging.debug("Day : %s", day)
                for _ in range(day):
                    ### This might be a bug, time must go forward in the list,
                    ### think it is randomized here.
                    ### How to distribute over hours / seconds ?
                    new_person = Person(year=str(self.time.year),\
                               month=str(month), day=str(day_number), \
                               country=self.country, \
                               father=self.males[random.randint(father_first_index, \
                                                                father_last_index)], \
                               mother=self.females[random.randint(mother_first_index, \
                                                                mother_last_index)])
                    person = deepcopy(new_person)
                    if person.is_male():
                        self.males.append(person)
                    else:
                        self.females.append(person)

                    self.time = datetime(year=self.time.year, month=month,\
                                         day=day_number, hour=23, minute=59)

        self.time = datetime(year=self.time.year+1, month=month, day=day_number, hour=23, minute=59)

        return True


    def get_age_range_index(self, persons=None, sex=None):
        """
        Get start and end index for each age range.
        Input list is a list of Persons

        Return a dictionary with : indexes[range]=(first,last)
        """
        indexes = {}
        ranges = iter(self.demographics.age_ranges)

        # Initiate indexes
        for from_age, to_age in ranges:
            (first, last) = self.age_range_index[sex][(from_age, to_age)]
            to_time = self.time - relativedelta(years=from_age)
            from_time = self.time - relativedelta(years=to_age)
            while persons[first].birthtime < from_time and first < len(persons) - 1:
                first += 1
            from_index = first
            while persons[last].birthtime <= to_time and last < len(persons)-1:
                last += 1
            indexes[from_age, to_age] = (from_index, last)

        return indexes


    def update_age_range_index(self):
        """Adjust age ranges when time moves"""
        for k in self.get_age_range_index(persons=self.males, sex='male'):
            print("Age Range Key : ", k)
        self.age_range_index['male'] = self.get_age_range_index(persons=self.males, \
                                                                sex='male')
        self.age_range_index['female'] = self.get_age_range_index(persons=self.females, \
                                                                  sex='female')


    def kill_persons_in_age_groups(self, sex=None, age_range=None, deaths=None):
        """
        Randomly kill a number of people in specified age group, deaths evenly
        distributed over last year.
        """
        logging.debug("kill_persons_in_age_groups Deaths : %s", deaths)
        if sex == 'male':
            persons = self.males
        else:
            persons = self.females

        (index_first, index_last) = self.age_range_index[sex][age_range]
        print("Age range ", age_range, ", first : ", index_first, " last : %s", index_last)

        for kill in range(math.ceil(deaths)):
            index = random.randint(index_first, index_last)
            #while persons[index].deathtime is not None:
            logging.debug("D = %s  Deaths : %s  Index = %s Deathtime : %s", kill, deaths,\
                          index, persons[index].deathtime)
            #    index = random.randint(index_first, index_last)

            logging.debug("Kill Person : %s", index)
            persons[index].deathtime = datetime(int(self.time.year), \
                                            random.randint(1, 12), \
                                            random.randint(1, 27), \
                                            random.randint(0, 23), \
                                            random.randint(0, 59), \
                                            random.randint(0, 59), 000)


    def apply_age_specific_death_rates(self, sex=None):
        """
        Apply age specific death rate to each age range.
        """
        age1 = age2 = 0
        logging.debug("Apply death rate")
        living = {}
        dead = {}
        if sex == 'male':
            indexes = self.get_age_range_index(persons=self.males, sex='male')
            persons = self.males
        else:
            indexes = self.get_age_range_index(persons=self.females, sex='female')
            persons = self.females
        # Count numnber of living persons.
        for age1, age2 in indexes:
            i_first, i_last = indexes[age1, age2]
            living[age1, age2] = 0
            dead[age1, age2] = 0
            logging.debug("Counting living males between %d and %d\
                           Index : %d to %d", age1, age2, i_first, i_last)
            for i in range(i_first, i_last-1):
                logging.debug("apply_age_specific_death_rates Indexes %s %s :\
                               %s Num Living : %d    Num Dead : %d", i, i_first, i_last,\
                               living[age1, age2], dead[age1, age2])
                print("persons[", i, "].deathtime : ", persons[i].deathtime)
                if not persons[i].deathtime:
                    living[age1, age2] += 1
                else:
                    dead[age1, age2] += 1

        kill = living[age1, age2] * self.demographics.death_rate[sex][(age1, age2)]
        self.kill_persons_in_age_groups(sex=sex, age_range=(age1, age2), deaths=kill)


    def add_couples(self, person1=None, person2=None):
        """Add two persons to the list of couples"""
        if not person1 or not person2:
            self.couples.add(person1, person2)


    def add_male(self, number):
        """
        Add a male to the population.
        """
        self.males.append(number)


    def delete_male(self, number):
        """
        Deleta male, death or migration.
        """

    def size(self):
        """Return the total population, living and dead"""
        return len(self.males) + len(self.females)


    def find_children(self, person=None):
        """
        If the person is a male, search person where their fathers is equal to
        this person, if the person is a female, search for persons where their
        mothers are equal to this persion.


        Return : A list of children.
        """
        if not person:
            return []

        children = []

        if person.is_male():
            for male in self.males:
                if male.father == person:
                    children.append(male)
            for female in self.females:
                if female.father == person:
                    children.append(female)
        else:
            for male in self.females:
                if male.mother == person:
                    children.append(male)
            for female in self.females:
                if female.mother == person:
                    children.append(female)

        return children


    def advance(self, years=1):
        """
        Use demographics to advance population specified time one year at the
        time.
        To advance one year, for example from 2017 to 2018, give birth to new
        persons for one year.
        Start at the first living person and use death rate kill people.

        1. Set time to self.time + one year.
        2. Give birth to childs using the birth rate during one year.
        3. Get start and end index of first age range (<older than>, younger than>)
        4. Execute birth rate for couples between 25 and 35
        5. Execute death rate for males and females in each age group.
        """
        for _ in range(years):
            # Add one year of new borned
            self.add_birth(months=12)

            male_range_index = self.get_age_range_index(self.males, sex='male')
            female_range_index = self.get_age_range_index(self.females, sex='female')

            # Apply death rate for all age ranges for males
            self.apply_age_specific_death_rates(sex='male')
            self.apply_age_specific_death_rates(sex='female')

            ranges = iter(self.demographics.age_ranges)

            for r_from, r_to in ranges:
                print("Male Index Ranges : form ", r_from, " to ", r_to, \
                      "  Indexes : ", male_range_index[r_from, r_to])
                print("Female Index Ranges : form ", r_from, " to ", r_to, \
                      "  Indexes : ", female_range_index[r_from, r_to])

        self.update_age_range_index()
        logging.debug("Advance End Time : %s", self.time)


    def add(self, population=None):
        """
        Merge population p to this population.

        1. Insert people in birth time order.
        2. If birthtime is exactly the same, random order.
        """
        males = []
        females = []

        if self.time != population.time:
            return False
        if population == self:
            return False

        mypop = new = 0

        while mypop < len(self.males) and new < len(population.males):
            if self.males[mypop].birthtime <= population.males[new].birthtime:
                males.append(self.males[mypop])
                mypop += 1
            else:
                males.append(population.males[new])
                new += 1

        self.males = males

        mypop = new = 0
        while mypop < len(self.females) and new < len(population.females):
            if self.females[mypop].birthtime <= population.females[new].birthtime:
                females.append(self.females[mypop])
                mypop += 1
            else:
                females.append(population.females[new])
                new += 1

        self.females = females
        self.update_age_range_index()


class ArgParse:
    """Parse command line parameters"""
    def __init__(self):
        parser = argparse.ArgumentParser(
            description='Generate personnummer.',
            formatter_class=RawTextHelpFormatter,
            epilog="\
Example\n\
python ./population.py -y 1999 -s 100000\n\n\
python ./population.py -y 1981 -m 12 -d 28 -c98 -s7")

        parser.add_argument('-l', '--log',
                            default=None,
                            action="store",
                            dest="loglevel",
                            help='Log level DEBUG, INFORMATION, WARNING, ERROR and CRITICAL')

        parser.add_argument('-y', '--year',
                            default=None,
                            action="store",
                            dest="year",
                            help='Year of birth, positive integer')

        parser.add_argument('-m', '--month',
                            default=1,
                            action="store",
                            dest="month",
                            help='Month of birth, 1 to 12')

        parser.add_argument('-d', '--day',
                            default=None,
                            action="store",
                            dest="day",
                            help='Day of birth.')

        parser.add_argument('-c', '--country',
                            default='sv',
                            action="store",
                            dest="country",
                            help='Define country.')


        parser.add_argument('-k', '--county',
                            default=None,
                            action="store",
                            dest="county",
                            help='Define county.')

        parser.add_argument('-s', '--sex',
                            default=None,
                            action="store",
                            dest="sex",
                            help='Define sex.')


        parser.add_argument('-r', '--ratio',
                            default='50:50',
                            action="store",
                            dest="ratio",
                            help='Ratio between males and females')

        parser.add_argument('-n', '--number',
                            default=100000,
                            action="store",
                            dest="number",
                            help='Generate n person numbers,\
                                  specifying -1966 means random before 1966')


        args = parser.parse_args()

        self.year = args.year
        self.month = args.month
        self.day = args.day
        self.country = args.country
        self.county = args.county
        self.sex = args.sex
        self.number = args.number
        self.loglevel = args.loglevel


def print_persons(persons=None):
    """Print population information"""
    print("Print population information")
    for person in persons:
        print("Get details for person ")
        print("First Name          : ", person.details.name.first)
        print("Second Name         : ", person.details.name.second)
        print("Surname             : ", person.details.name.surname)
        print("Sex                 : ", person.gender)
        print("Personnummer        : ", person.details.ssid)
        print("Street              : ", person.details.address.street, \
                                        person.details.address.street_number)
        print("City                : ", person.details.address.city_name)
        print("Zipcode             : ", person.details.address.zipcode)


def print_population(population=None):
    """Print summary of population"""
    print("Print summary of population")
    print("\nSummary : ", population.time.strftime("%Y-%m-%d"))
    print("Size of population   : ", population.size())
    print("Size of male population   : ", len(population.males))
    print("Size of female population   : ", len(population.females))
    print("Demograpics                 : ", population.demographics)


def main():
    """Command line interface for Population"""
    args = ArgParse()

    if args.loglevel:
        numeric_level = getattr(logging, args.loglevel.upper(), None)
    else:
        numeric_level = 0

    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % numeric_level)
    logging.basicConfig(filename='populate.log', filemode='w', level=numeric_level)
    logging.basicConfig(format='%(asctime)s %(message)s')

    sweden = Country(country=args.country)
    swedish_population = Population(country=sweden, begin=1901, population=5000)

    print_persons(persons=swedish_population.males + swedish_population.females)
    print_population(population=swedish_population)

    finland = Country(country='fi')
    finnish_population = Population(country=finland, begin=1901, population=5000)

    swedish_population.add(finnish_population)

    print_persons(persons=swedish_population.females)
    print_population(population=swedish_population)

    swedish_population.advance(years=1)
    swedish_population.advance(years=2)
    swedish_population.advance(years=7)
    swedish_population.advance(years=40)

    print("Summary of Swedish population\n=============================\n", swedish_population)

    print("Demograpics                 : ", swedish_population.demographics)

    print("Person        : ", swedish_population.males[len(swedish_population.males)-100])
    print("Person Father : ", swedish_population.males[len(swedish_population.males)-100].father)
    print("Person Mother : ", swedish_population.males[len(swedish_population.males)-100].mother)

    person = swedish_population.males[len(swedish_population.males)-2500].father
    print("Find children of : ", person)
    for child in swedish_population.find_children(person=person):
        print("Child : ", child)


if __name__ == '__main__':
    main()
