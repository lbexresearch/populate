# coding=utf-8
import random
import logging

class Common:
    """Common functions"""

    def __init__(self,logfile="populate.log", loglevel=logging.DEBUG):
        logging.basicConfig(filename=logfile,loglevel=logging.DEBUG)
        self.logfile=logfile
        self.loglevel=level
        logging.info('Initiated common variables')
        return true


def main():
    print("Name Mrs  : ")

    logging.basicConfig(filename='populate.log',level=logging.DEBUG)
    logging.debug('This message should go to the log file')
    logging.info('So should this')
    logging.warning('And this, too')


if __name__ == '__main__':
    main()
