#!/usr/bin/env python3
# coding=utf-8
"""Module for street names"""
import random
import argparse
from argparse import RawTextHelpFormatter

class Street:
    """Generate street names based on country code"""
    street = {}

    street['sv'] = {}
    street['fi'] = {}
    street['gb'] = {}

    street['sv']['street_pre'] = ["Öster",
                                  "Väster",
                                  "Norr",
                                  "Syd",
                                  "Kungs",
                                  "Prins",
                                  "Drottning",
                                  "Furst",
                                  "Biskops",
                                  "Pilgrims",
                                  "Präst",
                                  "Oden",
                                  "Tors",
                                  "Freja",
                                  "Balder",
                                  "Loke",
                                  "Stock",
                                  "Assar",
                                  "Göte",
                                  "Gustav",
                                  "Stor",
                                  "Ny",
                                  "Höjd",
                                  "Hag",
                                  "Träsk",
                                  "Sjö",
                                  "Tran",
                                  "Tärn",
                                  "Häger",
                                  "Gås",
                                  "Anke",
                                  "Finn",
                                  "Lång",
                                  "Stor",
                                  "Krog",
                                  "Kungs",
                                  "Liljeholms",
                                  "Nya",
                                  "Gamla",
                                  "Lugna",
                                  "Tysta",
                                  "Nybohovs"]

    street['sv']['street_ending'] = ["stigen",
                                     "kroken",
                                     "gatan",
                                     "vägen",
                                     "allén",
                                     "esplanaden",
                                     "boulevarden"]

    street['fi']['street_pre'] = ["Aleksanterin",
                                  "Tehtaan",
                                  "Korkeavuoren",
                                  "Assar"]

    street['fi']['street_ending'] = ["katu", "esplanadi", "boulevardi"]

    street['gb']['street_pre'] = ["King Willian ",
                                  "Kings ",
                                  "Queens ",
                                  "Prince ",
                                  "Princess ",
                                  "Lord Byron ",
                                  "Victoria ",
                                  "Regents ",
                                  "Liverpool ",
                                  "Oxford ",
                                  "Downham ",
                                  "Kingsland ",
                                  "Shoreditch ",
                                  "Hoxton ",
                                  "Hackney ",
                                  "Clapham ",
                                  "Stoke ",
                                  "Portabello "]

    street['gb']['street_ending'] = ["Street", "Road", "Row", "Close", "Market", "Lane", "Terasse"]


    name = ""
    count = 0

    def __init__(self, maxstreets=10, country='gb'):
        """Initiate Steet object"""
        self.max = maxstreets
        self.country = country
        self.name = self.streetname()


    def __iter__(self):
        self.count = 0
        return self

    def __repr__(self):
        return self.name

    def __next__(self):
        if self.count == self.max:
            raise StopIteration
        self.count += 1
        self.name = self.streetname()
        return self.name

    def streetname(self):
        """Return a random stree name"""
        return self.street[self.country]['street_pre']\
                [int(random.randint(0, len(self.street[self.country]['street_pre'])-1))] + \
               self.street[self.country]['street_ending']\
               [int(random.randint(0, len(self.street[self.country]['street_ending'])-1))]

    def set_streetname(self, name=None):
        """Set the street name"""
        self.name = name

    def get_streetname(self):
        """Get the street name"""
        return self.name


class ArgParse:
    """Parse command line arguments"""
    def __init__(self):

        parser = argparse.ArgumentParser(
            description='Generate streetname.',
            formatter_class=RawTextHelpFormatter,
            epilog="\
Example\n \
PYTHONPATH=/home/magnus/workspace/utilities/scripts/python/populate/ \
python3 street/street.py -c 'sv'")

        parser.add_argument('-c', '--country',
                            default='sv',
                            action="store",
                            dest="country",
                            help='Country code of street to generate')

        args = parser.parse_args()

        self.country = args.country

    def set_country(self, country=None):
        """Set country"""
        self.country = country

    def get_country(self):
        """Get country"""
        return self.country



def main():
    """Generate random streets"""
    args = ArgParse()

    print("\n\nUse iterator to generate street names, country code : ", args.country)
    for street in Street(maxstreets=20, country=args.country):
        print("Street : ", street)

    streets = Street(country=args.country, maxstreets=10)

    print("\n\nUse next to generate new street names")
    print(streets)
    print(next(streets))
    print(next(streets))


if __name__ == '__main__':
    main()
