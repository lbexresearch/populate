#!/usr/bin/env python3
"""This module handles differen aspects of personal id numbers."""
import argparse
import random
from argparse import RawTextHelpFormatter

class Personnummer:
    """Class Personnummmer, handles calculation of cecksums and so on"""
    def __init__(self, year=None, month=None, day=None, county=None, sex=None):
        if year and year[0] == '+':
            year = random.randint(int(year[1:]), 2017)
        elif year and year[0] == '-':
            year = random.randint(1900, int(year[1:]))
        elif not year:
            year = random.randint(1900, 2017)
        if not month:
            month = random.randint(1, 12)
        if not day:
            day = random.randint(1, 27)
        if not county:
            county = random .randint(1, 99)
        if not sex:
            sex = random.randint(0, 9)

        number = str(int(year) % 100).zfill(2) + str(month).zfill(2) + \
                str(day).zfill(2) + str(county).zfill(2) + str(sex)
        self.number = number + str((self.checksum(number)))

    def __str__(self):
        return self.number


    def __next__(self):
        print("Get next")

        while self.i < self.count:
            year = random.randint(1936, 2017)
            month = random.randint(1, 12)
            day = random.randint(1, 27)
            county = random .randint(1, 99)
            sex = random.randint(0, 9)
            print("Year : {}".format(year))
            self.i = self.i + 1
            yield int(Personnummer(year=year,
                                   month=month,
                                   day=day,
                                   county=county,
                                   sex=sex))

    def __iter__(self):
        return self


    def checksum(self, number):
        """The Luhn algorithm"""
        p = 0
        s = 0
        toggle = 2
        for char in number:
            p = int(char) * toggle
            while p:
                s = s + p % 10
                p //= 10
            if toggle == 1:
                toggle = 2
            else:
                toggle = 1

        return (10 - s % 10) % 10


    def validate_checksum(self, number=None):
        p = 0
        s1 = 0
        s2 = 0
        toggle = 2
        if not number:
            number = self.number

        for c in number:
            p = int(c) * toggle
            while p:
                s2 = s2 + p % 10
                p //= 10
            if toggle == 1:
                toggle = 2
            else:
                toggle = 1

        if (10 - s2 % 10) % 10:
            return False
        return True

    def show(self):
        """Show personnummer"""
        return str(self.number)


class Generate:

    def __init__(self, count=10):
        self.count = count

    def __iter__(self):
        for i in range(self.count):
            year = random.randint(1936, 2017)
            month = random.randint(1, 12)
            day = random.randint(1, 27)
            county = random .randint(1, 99)
            sex = random.randint(0, 9)
            yield Personnummer(year=str(year),
                               month=month,
                               day=day,
                               county=county,
                               sex=sex)

    def __str__(self):
        return "Number : %s"


class ArgParse:
    """Parse commandline arguments"""
    def __init__(self):
        parser = argparse.ArgumentParser(
            description='Generate personnummer.',
            formatter_class=RawTextHelpFormatter,
            epilog="\
Example\n\
python ./Personnummer.py -y -1999\n\n\
python ./Personnummer.py -y +1999\n\n\
python ./Personnummer.py -y 1981 -m 12 -d 28 -c98 -s7")

        parser.add_argument('-y', '--year',
                            default=None,
                            action="store",
                            dest="year",
                            help='Year of birth, positive integer')

        parser.add_argument('-m', '--month',
                            default=None,
                            action="store",
                            dest="month",
                            help='Month of birth, 1 to 12')

        parser.add_argument('-d', '--day',
                            default=None,
                            action="store",
                            dest="day",
                            help='Day of birth.')

        parser.add_argument('-c', '--county',
                            default=None,
                            action="store",
                            dest="county",
                            help='County of birth (before 1990)')

        parser.add_argument('-s', '--sex',
                            default=None,
                            action="store",
                            dest="sex",
                            help='Odd numbers for Males and even numbers for Female')


        parser.add_argument('-r', '--randomize',
                            default=False,
                            action="store_false",
                            dest="randomize",
                            help='Randomize a random person number,\
 specifying -1966 means random before 1966')

        parser.add_argument('-n', '--number',
                            default="1",
                            action="store",
                            dest="number",
                            help='Generate n person numbers, \
specifying -1966 means random before 1966')


        args = parser.parse_args()

        self.year = args.year
        self.month = args.month
        self.day = args.day
        self.county = args.county
        self.sex = args.sex
        self.number = args.number


def main():
    args = ArgParse()

    if args.number:
        for q in Generate(int(args.number)):
            # print("Get next : {}".format(q.show()))
            if q.validate_checksum():
                print("{} Checksum OK".format(q))
            else:
                print("{} Checksum NOK".format(q))
    else:
        p = Personnummer(year=args.year, month=args.month, day=args.day, \
                       county=args.county, sex=args.sex)
        p.validate_checksum()
        print("Number : {}".format((p.number)))
        print("P = {}".format(p.show()))

if __name__ == '__main__':
    main()
