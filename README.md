# Populate - Demograhic Calculations

Python3 Modules to simulate populations.

*A population is all the organisms of the same group or species, which live in a 
particular geographical area, and have the capability of interbreeding.*

In this case the organism is humans and the area is a fictional country.


[![pipeline status](https://gitlab.com/lbexresearch/populate/badges/master/pipeline.svg)](https://gitlab.com/lbexresearch/populate/commits/master)


## Test Coverage

- [![coverage report](https://gitlab.com/lbexresearch/populate/badges/master/coverage.svg)](https://gitlab.com/lbexresearch/populate/commits/master)



## Background


Modules for generating randomized populations to be used as test data.

    * Randomized populations and geographical areas.
    * Simulation of population growth using birth and death rates and immigration rates.
    * Ethnic groups and kinship.
    * Perform arithmetics on populations.
    * Genereate family trees.
    * Apply demographic constants to simulate diseases and war. 

### Example

Create a seed population of 10000000 inhabitants born in Sweden 1900-1930.
```python
sweden=Country(country=args.country)
swedish_population=Population(country=sweden,begin=1901, population=10000
```

This wil create  one generation of males and females with an age 0 to 30 year.
After this the population can be rolled forward yearly steps, demograhic
properties can be changed to reflect changes in birth and death rates.

There is a problem with a seed generation and the birthrate because that take
into account a full population with ages ranging from 0 to 100 years. So to 
start with I'll use the fertility rate for birth calculation. 

There is possible to simulate in- and out migration with + and -. 


*Sources :*

https://en.wikipedia.org/wiki/Kinship
https://en.wikipedia.org/wiki/Consanguinity
https://en.wikipedia.org/wiki/Consanguinity#/media/File:Coefficient_of_relatedness.png

http://download.geonames.org/export/zip/

#### Requirements


```
sudo apt install python3-pip
sudo pip3 install nose
sudo pip3 install coverage
sudo pip3 install NoseXUnit
sudo pip3 install setuptools
sudo pip3 install python-dateutil

```


#### How to start

##### Persons and family trees

All persons have parents, in the first generation they are None. At the moment
there is no relation between parents and childrens, I don't know if it is needed.

If you do not specify parents they will be randomly picked from the population
in the age range between 25 and 45.
```python
from country.country import Country
from person.person import Person
sweden=Country()
father=Person(country=sweden, year="1923", month=4,sex=1)
mother=Person(country=sweden, year="1928", month=5,sex=2)
child=Person(country=sweden, year="1945", month=5,sex=1,father=father,mother=mother)
print(child)
print(child.mother)
print(child.father)
```


##### Populations

First there must be an initial base population, normally one generation
without parents. After this the population can continue to growt.

    1. Define a country ( geographic area )
    2. Define a base generation, this is a generation without parents. It is
       by defining a ghost population, a start year and a growth rate.
       The default base generation time frame is 30 years.
    3. After this the base generation can start mating and produce own children
       at birth rate and die off at death rate.

To generate a base generation starting at 1900 and a fictional population of
1000000 people.

```python
p0=Population(p=1000000, growth_rate=1.3, year=1900)
```

This command will return a population of 1000000 distributed over one generation
e.g 30 years. 

http://www.columbia.edu/itc/hs/pubhealth/modules/demography/populationRates.html


The population is identified by a sequential list of persons at a specific
point in time. 

* The time is advanced forward, no time travel possible.
* Persons can be added to the past and up til now.
* Each person is identified by a 
    - Time of birth
    - Time of death
    - Sex, male/female
    - County, zipcode
    - Country. default 'sv'


Date and time 

```python
print datetime.date(2003, 8, 6) - datetime.date(2000, 8, 6)
```


### Population operators


```python
p1=Population('sv', year=1978, month=1, day=1, country=c)
p2=Population('sv')

p3=p1+p2

p4=p1.males()

p5=p4-p3

for q in p:
```
    

## Quick Examples

1. Create a geographical area
   country=Country(cities=100)

2. Create a population with birthplace and address in the country created in step 1.
   population=Population(country)

   personnummer = Personnummer(year=1966, month=07, day=27)

   population.add(Person())
   population.advance_time(days=3) 

   personaldetails.
   


To run the population.py script
```
cd ~/workspace/populate 
PYTHONPATH=. python3 population/population.py
```


Generate a city with 100 streets.
```
PYTHONPATH=. python3 city/city.py -s 100
```

To generate a country with 1000 cities (towns) run :
```
PYTHONPATH=. python3 country/country.py -t 1000
```


## Randomize Geograpical Areas

It could be interesting to implement the physical country. 
https://mewo2.com/notes/terrain/


## Generate a population.



This will create a population of 100000 people in the location Sweden borned
between 1900 and 1930.

p1=Population(year=1900, size=100000, quota=0.5, country='sv', countys=300)

p1.birthrate=37     # The total number of live births per 1,000 of a population in a year.

p1.step(month=6)    # Step the population 6 month forward.

print("Population %s at time %s)%(p1.population, p1.time)


```
~/workspace/utilities/scripts/python/identity $ PYTHONPATH=/home/magnus/workspace/utilities/scripts/python/identity python3.6 -tt population/population.py -y 2017 -n 1000000
```


### Names

The name class generates random names from different countries like Sweden, Finland and UK.


Some examples :
```
>>> n=Name(first='Elvis',gender='male')
>>> print(n)
Name    : Elvis Gustav Bjurström
>>> n=Name(first='Elvis',countrycode='fi', gender='male')
>>> print(n)
Name    : Elvis Veeti Järvläinen
```


### Location, Country, City and Street


Country, number of citys in the country 1-999 


```
from country.country import Country
sweden=Country()
```


The city class

```python
from city.city import City
c=City()
print("City %s")%( c)
City 40797 Sjövik
```



## Person

Attributes of persons :

```
Mother                 [person]
Father                 [None|person]
Personnummer           [Personnummer]
Birthdate              [yyyymmdd]
Deathdate              [None|yyyymmdd]
Birthplace             [city|contry]
Address                [address]
Spouse                 [person|None]
Status                 [Married/Widow/Divorced]
Children               [person,...]
```

### Relationships

Different relationships
```
Father
Mother
Son
Daughter
```

### Social staus


Married, divorced, children together.

### Family Tree

To generate a family tree, 



Numbers of Ancestors males and females


Define start year, for example 1900 and the size of the population.
4000 males and 100 females, 
Distribute over one generation default 30


A generation is all of the people born and living at about the same time, 
regarded collectively." It can also be described as, "the average period, 
generally considered to be about thirty years, during which children are
 born and grow up, become adults, and begin to have children of their own.i
In kinship terminology, it is a structural term designating the 
parent-child relationship. It is also known as biogenesis, reproduction,
or procreation in the biological sciences.

### Nativity


Number of kids per women

```
[{year}=number,...]
Birth rate
Death Rate
```

Imigration
Emigration


## Kinship Distance



## Development


### Use 



Run pylint for python 3
```python
sudo pip3 install pylint
pylint --version
No config file found, using default configuration
pylint 1.8.4, 
astroid 1.6.3
Python 3.5.2 (default, Nov 23 2017, 16:37:01) 
[GCC 5.4.0 20160609]
``` 

Run pylint like this
```bash
> PYTHONPATH=. pylint address/address.py 
No config file found, using default configuration
************* Module address
R: 48, 0: Too few public methods (1/2) (too-few-public-methods)
C:  8, 0: Imports from package argparse are not grouped (ungrouped-imports)

------------------------------------------------------------------
Your code has been rated at 9.49/10 (previous run: 9.49/10, +0.00)
```

[Google Python Style Guide](https://google.github.io/styleguide/pyguide.html)


b++++++++++++d----------------------
-----b++++++++++++++++++d-----------
------b++++++d----------------------
-------b+++++++++d------------------
-----------b+++++++++++++++++d------
           |       age  ->



# Testing

At the moment there are untitests that generates xunit test and code coverage
reports.

## Code Coverage

http://coverage.readthedocs.io/en/latest/config.html

## Unittest


Some example how to run unit tests.

```
cd populate/ 
# Run one test module
PYTHONPATH=.:test python3 -m unittest TestPopulation
# Run just a specific testcase
PYTHONPATH=.:test python3 -m unittest test_population.TestPopulation.test_age_ranges
```






### Docker

Run the unit tests in a docker container.
```
gitlab-runner exec docker unit-test
```

### Nosetests



Run all tests, pretty much silent
```
nostetests
```

Run a specific class.
```
PYTHONPATH=.:test nosetests TestPopulation
```

Some verbosity :
```
> PYTHONPATH=.:test nosetests -v TestPopulation
Generate a Swedish population ... ok
Generate a Swedish population ... ok
Advance a Swedish population 10 years ... ok
Use the age ranges, ... ok
Generate a Swedish population ... ok

----------------------------------------------------------------------
Ran 5 tests in 38.666s

OK
```




```
cd populate
PYTHONPATH=. python3 -m "nose"
```


Generate coverage, xuit, html reports
```
cd populate
nosetests --with-coverage --with-xunit --cover-tests --cover-html
```

### Notice

* Different time zones are not taken into account when calculating age.
