#!/usr/bin/env python3
"""This module handles city objects."""
# coding=utf-8
import random
import argparse
from argparse import RawTextHelpFormatter

class City:
    """City Class"""
    city = {}


    city['sv'] = {}
    city['fi'] = {}
    city['gb'] = {}

    city['sv']['city_pre'] = ["Öster",
                              "Väster",
                              "Norr",
                              "Syd",
                              "Kungs",
                              "Prins",
                              "Drottning",
                              "Furst",
                              "Biskop",
                              "Pilgrim",
                              "Präst",
                              "Stock",
                              "Assar",
                              "Göte",
                              "Gustav",
                              "Stor",
                              "Ny",
                              "Höjd",
                              "Hag",
                              "Träsk",
                              "Sjö",
                              "Tran",
                              "Tärn",
                              "Häger",
                              "Gås",
                              "Anke",
                              "Finn"]

    city['sv']['city_ending'] = ["arp",
                                 "böle",
                                 "sjö",
                                 "träsk",
                                 "by",
                                 "stad",
                                 "sund",
                                 "sele",
                                 "ö",
                                 "å",
                                 "vik",
                                 "skog",
                                 "skogen",
                                 "kulle",
                                 "berg",
                                 "mark",
                                 "näs",
                                 "mo",
                                 "sel",
                                 "äng",
                                 "ängen",
                                 "köping",
                                 "bygs",
                                 "holm",
                                 "hus",
                                 "torp",
                                 "kvarn",
                                 "borg",
                                 "ryd",
                                 "red",
                                 "tälje"]

    #  Finish City name components

    city['fi']['city_pre'] = ["Maarian",
                              "Lammas",
                              "Leppä",
                              "Arabian"]

    city['fi']['city_ending'] = ["hamina",
                                 "saari",
                                 "salmi",
                                 "vaara",
                                 "ranta"]


    city['gb']['city_pre'] = ["Chester",
                              "North",
                              "South",
                              "West",
                              "East",
                              "Up",
                              "Upper",
                              "Down",
                              "End"]

    city['gb']['city_ending'] = ["ester",
                                 "by",
                                 "ham",
                                 "gay",
                                 "say",
                                 "hampton",
                                 "ington",
                                 "dale",
                                 "port",
                                 "wood",
                                 "field",
                                 "land",
                                 "ford",
                                 "hurst",
                                 "borough",
                                 "ton",
                                 "mill",
                                 "more",
                                 "worth",
                                 "falk"]


    zip_counter = 10000
    zipcode = 10000
    name = None
    count = 0

    def __init__(self, country='sv', cities=5):
        self.country = country
        self.max = cities
        self.streets = dict()

    def __iter__(self, country='sv'):
        return self


    def __next__(self):
        if self.count == self.max:
            raise StopIteration
        self.count += 1
        self.zipcode = self.set_zipcode()
        self.name = self.cityname()
        self.streets[self.name] = []
        return self

    def __str__(self):
        return str(self.zipcode)  + " " + self.name


    def set_zipcode(self):
        """Zipcodes are increases one by one."""
        self.zipcode = self.zipcode + 1
        return self.zipcode


    def get_zipcode(self):
        """Return Zip Code"""
        return self.zipcode

    def cityname(self):
        """Get a random city name"""
        return  self.city[self.country]['city_pre']\
                [int(random.randint(0, len(self.city[self.country]['city_pre'])-1))] + \
                self.city[self.country]['city_ending']\
                [int(random.randint(0, len(self.city[self.country]['city_ending'])-1))]

    def add_street(self, city=None, street=None):
        """Append a street name to city street list"""
        self.streets[city].append(street)

    def get_street(self, index=0):
        """Get street with specified index"""
        return self.streets[index]


class ArgParse:
    """Parse command line arguments"""
    def __init__(self):
        parser = argparse.ArgumentParser(
            description='Generate City.',
            formatter_class=RawTextHelpFormatter,
            epilog="\
Example\n\
python3.6 city.py -c 100 -p 100000\n\n\
Create a country with 100 cities and a population of about 100000")

        parser.add_argument('-c', '--country',
                            default='sv',
                            action="store",
                            dest="country",
                            help='Country')

        parser.add_argument('-s', '--streets',
                            default=10,
                            action="store",
                            dest="streets",
                            help='Number of cities')

        args = parser.parse_args()

        self.country = args.country
        self.streets = args.streets


def main():
    """Command Line Interface"""
    import copy
    from street.street import Street

    args = ArgParse()

    print("City Generator for country ", args.country)
    cities = []

    for next_city in City(cities=5, country=args.country):
        print("Zip 1 : ", next_city.zipcode, "   Name : ", next_city.name)
        city = copy.copy(next_city)
        for next_street in Street(maxstreets=random.randint(1, int(args.streets)),\
                                  country=args.country):
            print("Street : ", next_street)
            street = copy.copy(next_street)
            city.add_street(city=next_city.name, street=street)

        cities.append(city)

    for city in cities:
        print("Zip 2 : ", city.zipcode, "   Name : ", city.name)
        for street in city.streets[city.name]:
            print("     ", street)

    print("City 1 : ", cities[1])
    print("City 4 : ", cities[4])


if __name__ == '__main__':
    main()
