#!/usr/bin/env python3
"""This module handles Address objects.

An address is a street in city with a zipcode in a country.
"""
import random
import sys
import argparse
from argparse import RawTextHelpFormatter
from country.country import Country

class Address(object):
    """This class deals with Address objects"""

    def __init__(self, street=None, city=None, country=None):

        cities = len(country.cities)
        if city:
            self.city = city
        else:
            self.city = country.cities[random.randint(0, cities-1)]

        self.city_name = self.city.name
        self.zipcode = self.city.zipcode

        if street:
            self.street = street
        else:
            self.street = self.city.streets[self.city.name]\
                [random.randint(0, len(self.city.streets[self.city.name]))-1]
        self.street_number = random.randint(1, 500)


    def rename_street(self, street=None):
        """Rename the street"""
        self.street = street

    def rename_zipcode(self, zipcode=None):
        """Change the zip code"""
        self.zipcode = zipcode

    def __str__(self):
        return "        Address  Street    : " + \
                self.street + " " + str(self.street_number) +  "\n" + \
               "                 Zip       : " + \
                str(self.zipcode) + " " + self.city_name


class ArgParse(object):
    """Parse commandline arguments"""
    def __init__(self):

        parser = argparse.ArgumentParser(
            description='Generate a random address.',
            formatter_class=RawTextHelpFormatter,
            epilog="\
Example\n\
PYTHONPATH=. python3 address/address.py -c 'gb'")

        parser.add_argument('-c', '--country',
                            default='sv',
                            action="store",
                            dest="country",
                            help='Country code of country to generate')

        self.args = parser.parse_args()

        self.country = self.args.country

    def print_arguments(self):
        """Print arguments"""
        print(self.args)

    def print_country(self):
        """If something goes wrong print help and exit 2"""
        print(self.country)
        sys.exit(2)

def main():
    """Command line address utillity"""
    args = ArgParse()

    country = Country(args.country)

    address = Address(country=country)

    print("2.1 City       : ", address.city.name)
    print("2.2 Street     : ", address.street)
    print("2.3 Street Num : ", address.street_number)

if __name__ == '__main__':
    main()
