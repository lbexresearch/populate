#!/usr/bin/env python3
"""Modeule for handeling different aspects of names"""
# coding=utf-8
import random
import json

class Name:
    """Class for name generation"""
    name = {}
    name['sv'] = {}
    name['fi'] = {}
    name['gb'] = {}

    with open('name/sv_female_names.json', 'r') as filehandle:  
        name['sv']['female'] = json.load(filehandle)

    with open('name/sv_male_names.json', 'r') as filehandle:  
        name['sv']['male'] = json.load(filehandle)

    with open('name/sv_surname_pre.json', 'r') as filehandle:  
        name['sv']['surname_pre'] = json.load(filehandle)

    with open('name/sv_surname_post.json', 'r') as filehandle:
        name['sv']['surname_post'] = json.load(filehandle)

    with open('name/fi_female_names.json', 'r') as filehandle:
        name['fi']['female'] = json.load(filehandle)

    with open('name/fi_male_names.json', 'r') as filehandle:
        name['fi']['male'] = json.load(filehandle)

    with open('name/fi_surname_pre.json', 'r') as filehandle:  
        name['fi']['surname_pre'] = json.load(filehandle)


    with open('name/fi_surname_post.json', 'r') as filehandle:
        name['fi']['surname_post'] = json.load(filehandle)

    with open('name/gb_male_names.json','r') as filehandle:
        name['gb']['male'] = json.load(filehandle)

    with open('name/gb_female_names.json','r') as filehandle: 
        name['gb']['female'] = json.load(filehandle)

    with open('name/gb_surnames.json','r') as filehandle:
        name['gb']['surname_pre'] = json.load(filehandle)
    name['gb']['surname_post'] = ['']


    def __init__(self, countrycode='sv', gender=None, first=None, second=None, surname=None):
        """
        Generate a name, first second and surname.
        """
        self.countrycode = countrycode
        self.first = "first"

        if not gender:
            gender = ["male", "female"][random.randint(0, 1)]

        if first:
            self.first = first
        else:
            self.first = self.name[countrycode][gender]\
                                  [int(random.randint(0, len(self.name[countrycode][gender])-1))]

        if second:
            self.second = second
        else:
            self.second = self.name[countrycode][gender]\
                        [int(random.randint(0, len(self.name[countrycode][gender])-1))]

        if surname:
            self.surname = surname
        else:
            self.surname = self.name[countrycode]['surname_pre']\
                                  [int(random.randint(0,\
                                   len(self.name[countrycode]['surname_pre'])-1))] + \
                         self.name[countrycode]['surname_post']\
                                  [int(random.randint(0,\
                                       len(self.name[countrycode]['surname_post'])-1))]


    def __str__(self):
        return "Name    : " + str(self.first) + " " + str(self.second) + " " + str(self.surname)


    def firstname(self, gender=None):
        """Returns a random name"""
        if not gender:
            gender = ["male", "female"][random.randint(0, 1)]
        return self.name[self.countrycode]\
                        [gender][int(random.randint(0,\
                        len(self.name[self.countrycode][gender])-1))]


    def surname(self):
        """Return random surname"""
        return self.name[self.countrycode]['surname_pre']\
                        [int(random.randint(0, len(self.name[self.countrycode]\
                        ['surname_pre'])-1))] + \
               self.name[self.countrycode]['surname_post']\
                        [int(random.randint(0, len(self.name[self.countrycode]\
                        ['surname_post'])-1))]


def main():
    """Command line interface"""
    name = Name(gender="male")
    print("Name Mr   : ", name.first, name.second, name.surname)
    name = Name(gender="female")
    print("Name Mrs  : ", name.first, name.second, name.surname)

    name = Name(countrycode='fi', gender="male")
    print("Name Mr   : ", name.first, name.second, name.surname)
    name = Name(countrycode='fi', gender="female")
    print("Name Mrs  : ", name.first, name.second, name.surname)

    name = Name(countrycode='gb', gender="male")
    print("Name Mr   : ", name.first, name.second, name.surname)
    name = Name(countrycode='gb', gender="female")
    print("Name Mrs  : ", name.first, name.second, name.surname)




if __name__ == '__main__':
    main()
