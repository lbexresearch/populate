#!/usr/bin/env python3
"""Country module, creates cities and streets"""
# coding=utf-8
import argparse
from argparse import RawTextHelpFormatter
import random
import copy
from city.city import City
from street.street import Street


class Country(object):
    """Country Class"""
    country_list = {'sv' : "Sverige",
                    'fi' : "Finland",
                    'gb' : "Great Britain"}


    def __init__(self, country='sv', cities=10, streets=10):

        self.country = self.country_list[country]
        self.code = country
        self.cities = []

        for next_city in City(cities=int(cities), country=country):
            city = copy.copy(next_city)
            max_streets = random.randint(1, int(streets))
            for next_street in Street(maxstreets=max_streets, country=self.code):
                street = copy.copy(next_street)
                city.add_street(city=next_city.name, street=street)

            self.cities.append(city)

    def __str__(self):
        return "Country          : " + self.country_list[self.code] + \
                "\nNumber of cities : " + str(len(self.cities))


    def __repr__(self):
        return self.country

    def name(self):
        """Print name of country"""
        return self.country

    def add_city(self, city):
        """Add a city to a country"""
        self.cities.append(city)

    def get_city(self, index=None):
        """Return city with index"""
        if not index:
            return len(self.cities)
        return self.cities[index]

class ArgParse:
    """Parse command line arguments"""
    def __init__(self):
        parser = argparse.ArgumentParser(
            description='Generate Country.',
            formatter_class=RawTextHelpFormatter,
            epilog="\
Example\n\
python3.6 country.py -c 100 -p 100000\n\n\
Create a country with 100 cities and a population of about 100000")

        parser.add_argument('-c', '--country',
                            default='sv',
                            action="store",
                            dest="country",
                            help='Country')

        parser.add_argument('-t', '--towns',
                            default=1,
                            action="store",
                            dest="cities",
                            help='Number of cities/towns')

        parser.add_argument('-s', '--streets',
                            default=10,
                            action="store",
                            dest="streets",
                            help='Number of cities')

        args = parser.parse_args()

        self.country = args.country
        self.cities = args.cities
        self.streets = args.streets

    def get_country(self):
        """Get country sent as argument"""
        return self.country

    def set_country(self, country=None):
        """Set the agrument country"""
        self.country = country

def main():
    """Generate a randomized country"""
    args = ArgParse()

    country = Country(country=args.country, streets=args.streets, cities=args.cities)


    for city in country.cities:
        print("City : ", city.zipcode, city.name)
        for street in city.streets[city.name]:
            print("     ", street)

    print(country)

if __name__ == '__main__':
    main()
