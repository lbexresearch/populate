#!/usr/bin/env python3
"""Handles a person in a population"""
import random
import datetime
import argparse
from argparse import RawTextHelpFormatter
from country.country import Country
from address.address import Address
from name.name import Name
from personnummer.personnummer import Personnummer


class Details:
    """
    Detailed information about a person. Birth date and time.
    Sex biological
    Gender social and cultural
    Address
    Email address
    Phonenumber
    Personal Number, social security number.
    """
    def __init__(self, country=None, county=None, gender=None, ssid=None):
        if not country:
            raise ValueError

        if not county:
            county = random.randint(0, 99)

        if not gender:
            gender = ["male", "female"][random.randint(0, 1)]

        self.country_of_origin = country
        self.county_of_origin = county
        self.gender = gender

        self.address = Address(country=country)
        self.name = Name(countrycode=country.code, gender=self.gender)
        self.ssid = ssid


    def __str__(self):
        return "        Details : Country of origin  : " + \
                str(self.country_of_origin) + \
               "\n        " + \
               "Details : County             : " + str(self.county_of_origin) + \
               "\n        " + \
               "Details : Social Security No : " + str(self.ssid) + \
               "\n        " + \
               str(self.name) + "\n"  + \
               str(self.address)


    def __repr__(self):
        return "Details: " + self.country_of_origin


    @classmethod
    def __next__(cls):
        """Get next details"""
        while True:
            year = random.randint(1936, 2017)
            month = random.randint(1, 12)
            day = random.randint(1, 27)
            county = random .randint(1, 99)
            sex = random.randint(0, 9)
            print("Year : {}".format(year))
            # self.i = self.i + 1
            yield int(Personnummer(year=year,
                                   month=month,
                                   day=day,
                                   county=county,
                                   sex=sex))

    def __iter__(self):
        return self

    def show_gender(self):
        """Show birthtime"""
        return str(self.gender)





class Person:
    """
    Minimal information about a person like time of birth, country, county and sex.
    """
    gender_list = ["female", "male"]

    def __init__(self, year=None, month=None, day=None, \
                 hour=None, minute=None, second=None, \
                 country=None, county=None, sex=None, mother=None, father=None):

        if year and year[0] == '+':
            year = str(random.randint(int(year[1:]), 2017))
        elif year and year[0] == '-':
            year = str(random.randint(1900, int(year[1:])))
        elif not year:
            year = str(random.randint(1900, 2017))
        if not month:
            month = random.randint(1, 12)
        if not day:
            day = random.randint(1, 27)
        if not hour:
            hour = random.randint(0, 23)
        if not minute:
            minute = random.randint(0, 59)
        if not second:
            second = random.randint(0, 59)
        if not country:
            print("Creating default Sweden")
            country = Country('sv')
        if not county:
            county = random.randint(0, 99)
        if not sex:
            self.sex = random.randint(0, 9)
        else:
            self.sex = sex

        self.mother = mother
        self.father = father

        personnummer = Personnummer(year=year,
                                    month=month,
                                    day=day,
                                    county=county,
                                    sex=sex)

        self.gender = self.gender_list[self.sex % 2]
        self.details = Details(country, gender=self.gender, ssid=personnummer)

        self.birthtime = datetime.datetime(int(year),
                                           int(month),
                                           int(day),
                                           int(hour),
                                           int(minute),
                                           int(second), 000)
        self.deathtime = None
        self.country_of_origin = country
        self.county_of_origin = county


    def __str__(self):
        return "Person: " + "\n    " + \
                "Birth Date    : " + str(self.birthtime) + "\n    " + \
                "Birth Country : " + str(self.country_of_origin) + "\n    " + \
                "Birth County  : " + str(self.county_of_origin) + "\n    " + \
                "Birth Sex     : " + str(self.sex) + "\n    " + \
                "Birth Gender  : " + str(self.gender) + "\n" + \
                str(self.details) + "\n"

    @classmethod
    def __next__(cls):
        """Get next person"""
        while True:
            year = random.randint(1936, 2017)
            month = random.randint(1, 12)
            day = random.randint(1, 27)
            county = random .randint(1, 99)
            sex = random.randint(0, 9)
            yield (Personnummer(year=year,
                                month=month,
                                day=day,
                                county=county,
                                sex=sex))

    def __iter__(self):
        return self


    def is_male(self):
        """Return True if male"""
        return self.sex  % 2

    def show(self):
        """Show birthtime"""
        return str(self.birthtime)


class ArgParse:
    """Parse arguments"""
    def __init__(self):
        parser = argparse.ArgumentParser(
            description='Generate personnummer.',
            formatter_class=RawTextHelpFormatter,
            epilog="\
Example\n\
python person/person.py -y -1999\n\n\
python person/person.py -y +1999\n\n\
python person/person.py -y 1981 -m 12 -d 28 -k98 -s7")

        parser.add_argument('-y', '--year',
                            default=None,
                            action="store",
                            dest="year",
                            help='Year of birth, positive or negative integer')

        parser.add_argument('-m', '--month',
                            default=None,
                            action="store",
                            dest="month",
                            help='Month of birth, 1 to 12')

        parser.add_argument('-d', '--day',
                            default=None,
                            action="store",
                            dest="day",
                            help='Day of birth.')

        parser.add_argument('-c', '--country',
                            default='sv',
                            action="store",
                            dest="country",
                            help='Country of birth')

        parser.add_argument('-k', '--county',
                            default=None,
                            action="store",
                            dest="county",
                            help='County of birth (before 1990)')

        parser.add_argument('-s', '--sex',
                            default=None,
                            action="store",
                            dest="sex",
                            help='Odd numbers for Males and even numbers for Female')


        args = parser.parse_args()

        self.year = args.year
        self.month = args.month
        self.day = args.day
        self.country = args.country
        self.county = args.county
        self.sex = args.sex

    def print_arguments(self):
        """Print arguments"""
        print("Arguments : ", self.year)

    def set_year(self, year=None):
        """Set argument year"""
        self.year = year


def main():
    """Command line interface"""
    args = ArgParse()

    country = Country(args.country)

    if args.sex:
        args.sex = int(args.sex)

    person = Person(country=country, year=args.year, \
               month=args.month, \
               day=args.day, \
               county=args.county, \
               sex=args.sex)

    print(person)


if __name__ == '__main__':
    main()
