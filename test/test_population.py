import unittest
import random
import sys
import time
import math
from dateutil.relativedelta import relativedelta
from population.population import Population
from country.country import Country

def setUpModule():
    print("Setup Module")
    # seed=1234


class TestPopulation(unittest.TestCase):
    """Test population class"""

    @classmethod
    def setupClass(cls):
        cls.seed = random.randrange(sys.maxsize)
        random.seed(cls.seed)
        print("Seeding using seed ",cls.seed)

    def setup_module(self):
        print("Start Population Test")

    def setUp(self):
        """Setup test"""
        print("Test : ", self.id()) 

    def tearDown(self):
        """Tear down test"""
        print("Number of testcases : ", self.countTestCases())

    def test_swedish_population(self):
        """Generate a Swedish population"""
        sweden=Country('sv')
        swedish_population=Population(country=sweden,begin=1901, population=10000)

        print("Population : ", swedish_population)

    def test_age_ranges(self):
        """Verify Population.get_age_range_index ranges, get statistics from age ranges, check age of first and last"""
        sweden=Country(country='sv')
        swedish_population=Population(country=sweden,begin=1901, population=1000)
        print("swedish_population.size = ",swedish_population.size())
        print("swedish_population year = ",swedish_population.time)
        self.assertGreater(swedish_population.size(), 8900, "Population size should be 8900")
        self.assertEqual(swedish_population.time.year, 1930, "Year should be start year + 30")
        age_index=swedish_population.get_age_range_index(persons=swedish_population.males, sex='male')
        # for r in age_index:
        for r in swedish_population.demographics.age_ranges:
            print("Test : ", swedish_population.demographics.age_range_description[r])
            print("Age Range : ", age_index[r])
            (youngest, oldest)=r
            (first,last) = age_index[r]
            print("Youngest and Oldest", youngest, oldest)
            print("First and Last", first, last)
            if first == last:
                print("Empty age Range")
                continue
            print("First birthdate : ",swedish_population.males[first].birthtime)
            print("Last birthdate : ",swedish_population.males[last].birthtime)
            print("First Age : ", swedish_population.time.year - swedish_population.males[first].birthtime.year)
            self.assertLessEqual( swedish_population.time.year - swedish_population.males[first].birthtime.year, oldest)
            print("Last Age : ", swedish_population.time.year - swedish_population.males[last].birthtime.year)
            self.assertGreaterEqual(youngest, swedish_population.time.year - swedish_population.males[last].birthtime.year)

        print("Demography : ", swedish_population.demographics) 


    def test_add_population(self):
        """Generate a Swedish population and a finish population"""
        sweden=Country('sv')
        finland=Country('fi')
        swedish_population=Population(country=sweden,begin=1901, population=10000)
        finish_population=Population(country=finland,begin=1901, population=5000)
        swedish_population.add(finish_population)
        print("Populaltion ", swedish_population)

    def test_add_population_small_big(self):
        """Generate a Swedish population"""
        sweden=Country('sv')
        finland=Country('fi')
        swedish_population=Population(country=sweden,begin=1901, population=10000)
        finish_population=Population(country=finland,begin=1901, population=15000)
        swedish_population.add(finish_population)
        print("Populaltion ", swedish_population)

    def test_advance_population(self):
        """Advance a Swedish population 10 years"""
        sweden=Country('sv')
        swedish_population=Population(country=sweden,begin=1901, population=10000)
        swedish_population.advance(years=10)
        print("Populaltion ", swedish_population)


    def test_person_children_father(self):
        """Find the children of a father"""
        sweden=Country('sv')
        swedish_population=Population(country=sweden,begin=1901, population=10000)
        swedish_population.advance(years=80)
        p=swedish_population.males[len(swedish_population.males)-100].father
        p1=swedish_population.males[len(swedish_population.males)-101].father
        print("Children of : ", p)
        for c in swedish_population.find_children(person=p):
                    self.assertEqual(c.father,p,"This should be the child of p")
                    self.assertNotEqual(c.father,p1,"This should not be the child of p1")
                    print("Child : ", c)

    def test_person_children_mother(self):
        """Find the children of a mother"""
        sweden=Country('sv')
        swedish_population=Population(country=sweden,begin=1901, population=10000)
        swedish_population.advance(years=80)
        p=swedish_population.males[len(swedish_population.males)-100].mother
        p1=swedish_population.males[len(swedish_population.males)-101].mother
        print("Children of : ", p)
        for c in swedish_population.find_children(person=p):
                    self.assertEqual(c.mother,p,"This should be the child of p")
                    self.assertNotEqual(c.mother,p1,"This should not be the child of p1")
                    print("Child : ", c)

       
