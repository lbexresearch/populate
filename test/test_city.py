import unittest
import random
import sys
import time
import math
from city.city import City

def setUpModule():
    print("Setup Module")


class TestName(unittest.TestCase):
    """Test name generator"""
    @classmethod
    def setupClass(cls):
        cls.seed = random.randrange(sys.maxsize)
        random.seed(cls.seed)
        print("setuUp, using seed : ",cls.seed)

    def setup_module(self):
        print("Start City Test")

    def setUp(self):
        """Setup test"""
        print("Test : ", self.id()) 

    def tearDown(self):
        """Tear down test"""
        print("Number of testcases and seed", self.countTestCases(), self.seed)

    def test_generate_city(self):
        """Generate a default City"""
        c=City()
        f=str(c.cityname())
        self.assertGreater(len(f),4, "This name is too short")
        self.assertEqual('sv', c.country)
        self.assertGreaterEqual(c.zipcode, 10000)
        self.assertLessEqual(c.zipcode, 99999)
        print("Random Swedish city name : ", f)


    def test_generate_city_fi(self, gender="male"):
        """Generate a male name"""
        c=City(country='fi')
        f=str(c.cityname())
        self.assertGreater(len(f),4, "This name is too short")
        self.assertEqual('fi', c.country)
        self.assertGreaterEqual(c.zipcode, 10000)
        self.assertLessEqual(c.zipcode, 99999)
        print("Random Finish city name : ", f)
