import unittest
import random
import sys
import time
import math
from person.person import Person
from country.country import Country

def setUpModule():
    print("Setup Module")
    # seed=1234


class TestName(unittest.TestCase):
    """Test person class"""

    @classmethod
    def setupClass(cls):
        cls.seed = random.randrange(sys.maxsize)
        random.seed(cls.seed)
        print("Seeding using seed ",cls.seed)

    def setup_module(self):
        print("Start Person Test")

    def setUp(self):
        """Setup test"""
        print("Test : ", self.id()) 

    def tearDown(self):
        """Tear down test"""
        print("Number of testcases : ", self.countTestCases())

    def test_generate_random_person(self):
        """Generate a male person"""
        c=Country('sv')
        p=Person(country=c)
        self.assertGreater(int(p.birthtime.strftime("%Y")) ,0, "Year can't be a negative number")
        self.assertLess(int(p.birthtime.strftime("%Y")),9999, "")
        print("Person : ", p)


    def test_generate_random_female_person(self):
        """Generate a female person"""
        c=Country('sv')
        p=Person(year="1923",sex=2, country=c)
        self.assertGreater(int(p.birthtime.strftime("%Y")),0, "Year can't be a negative number")
        self.assertLess(int(p.birthtime.strftime("%Y")) ,9999, "Year can't be a negative number")
        print("Person : ", p)


    def test_generate_random_male_person(self):
        """Generate a male person"""
        c=Country('sv')
        p=Person(year="1956", sex=1, country=c)
        self.assertEqual(int(p.birthtime.strftime("%Y")) ,1956, "Year can't be a negative number")
        self.assertLess(int(p.birthtime.strftime("%Y")) ,9999, "Year can't be a negative number")
        print("Person : ", p)

    def test_generate_random_fi_female_person(self):
        """Generate a female person"""
        c=Country('fi')
        p=Person(year="1923",sex=2, country=c)
        self.assertEqual(int(p.birthtime.strftime("%Y")) ,1923, "Year can't be a negative number")
        self.assertLess(int(p.birthtime.strftime("%Y")) ,9999, "Year can't be a negative number")
        print("Person : ", p)


    def test_generate_random_fi_male_person(self):
        """Generate a male person"""
        c=Country('fi')
        p=Person(year="1956", sex=1, country=c)
        self.assertEqual(int(p.birthtime.strftime("%Y")) ,1956, "Year can't be a negative number")
        self.assertLess(int(p.birthtime.strftime("%Y")) ,9999, "Year can't be a negative number")
        print("Person : ", p)
