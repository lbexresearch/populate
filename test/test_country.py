import unittest
import random
import sys
import time
import math
from country.country import Country

def setUpModule():
    print("Setup Module")


class TestName(unittest.TestCase):
    """Test name generator"""
    @classmethod
    def setupClass(cls):
        cls.seed = random.randrange(sys.maxsize)
        random.seed(cls.seed)
        print("setuUp, using seed : ",cls.seed)

    def setup_module(self):
        print("Start Country Test")

    def setUp(self):
        """Setup test"""
        print("Test : ", self.id()) 

    def tearDown(self):
        """Tear down test"""
        print("Number of testcases and seed", self.countTestCases(), self.seed)

    def test_generate_country(self):
        """Generate a default City"""
        c=Country()
        self.assertEqual('Sverige', c.country)
        self.assertEqual(len(c.cities), 10)
        print("Random Swedish city name : ", c.cities[1].name)


    def test_generate_city_fi(self):
        """Generate a male name"""
        c=Country(country='fi')
        self.assertEqual('Finland', c.country)
        self.assertEqual(len(c.cities), 10)
        print("Random Finish city name : ", c.cities[1].name)
