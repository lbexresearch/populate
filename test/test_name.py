import unittest
import random
import sys
import time
import math
from name.name import Name

def setUpModule():
    print("Setup Module")
    # seed=1234


class TestName(unittest.TestCase):
    """Test name generator"""

    @classmethod
    def setupClass(cls):
        cls.seed = random.randrange(sys.maxsize)
        random.seed(cls.seed)
        print("Seeding using seed ",cls.seed)

    def setup_module(self):
        print("Start Naming Test")

    def setUp(self):
        """Setup test"""
        print("Test : ", self.id()) 

    def tearDown(self):
        """Tear down test"""
        print("Number of testcases : ", self.countTestCases())

    def test_generate_random_name(self):
        """Generate a random name"""
        n=Name()
        print("Random name : ", n)
        self.assertGreater(len(n.first),1, "This name is too short")

    def test_generate_male_name(self):
        """Generate a random male name"""
        n=Name(gender="male")
        print("Male Name : ", n.first)
        self.assertGreater(len(n.first),1, "This name is too short : " + n.first)

    def test_generate_female_name(self):
        """Generate a random female name"""
        n=Name(gender="male")
        print("Female Name : ", n.first)
        self.assertGreater(len(n.first),1, "This name is too short : " + n.first)

    ### Swedish names


    def test_generate_swedish_name(self):
        """Generate random swedish name"""
        n=Name(countrycode='sv')
        print("Swedish random name : ", n)
        self.assertGreater(len(n.first),1, "This name is too short")


    def test_generate_swedish_male_name(self):
        """Generate a swedish male name"""
        n=Name(gender="male", countrycode='sv')
        print("Male Name : ", n.first)
        self.assertGreater(len(n.first),1, "This name is too short : " + n.first)

    def test_generate_swedish_male_name_birger(self):
        """Generate a swedish male name with first name Birger"""
        n=Name(gender="male", countrycode='sv', first="Birger" )
        print("Male Name : ", n.first)
        self.assertEqual(n.first,'Birger', "This name should be Birger : " + n.first)

    def test_generate_swedish_male_second_name_ingemar(self):
        """Generate a swedish male name with first name Ingemar"""
        n=Name(gender="male", countrycode='sv', second="Ingemar" )
        print("Male Name : ", n.second)
        self.assertEqual(n.second,'Ingemar', "The second name should be Ingemar : " + n.second)

    def test_generate_swedish_male_last_name_svensson(self):
        """Generate a swedish male name with surname Svensson"""
        n=Name(gender="male", countrycode='sv', surname ="Svensson" )
        print("Surname : ", n.surname)
        self.assertEqual(n.surname,'Svensson', "The surname should be Svensson : " + n.surname)

    def test_generate_swedish_male_full_name(self):
        """Generate a swedish full male name with surname Bergling"""
        n=Name(gender="male", countrycode='sv', first='Stig', second='Eugen', surname ="Bergling" )
        print("First : ", n.first, "Second : ", n.second, "  Surname: ", n.surname)
        self.assertEqual(n.first,'Stig', "The first name should be Stig : " + n.first)
        self.assertEqual(n.second,'Eugen', "The second name should be Eugen : " + n.second)
        self.assertEqual(n.surname,'Bergling', "The surname should be Bergling : " + n.surname)

    ### Swedish Females

    def test_generate_swedish_female_name(self):
        """Generate a swedish female name"""
        n=Name(gender="female", countrycode='sv')
        print("Female Name : ", n.first)
        self.assertGreater(len(n.first),1, "This name is too short : " + n.first)

    def test_generate_swedish_female_name_birger(self):
        """Generate a swedish female name with first name Berit"""
        n=Name(gender="female", countrycode='sv', first="Berit" )
        print("Female Name : ", n.first)
        self.assertEqual(n.first,'Berit', "This name should be Berit : " + n.first)

    def test_generate_swedish_female_second_name_ingemar(self):
        """Generate a swedish female name with first name Ingrid"""
        n=Name(gender="female", countrycode='sv', second="Ingrid" )
        print("Female Name : ", n.second)
        self.assertEqual(n.second,'Ingrid', "The second name should be Ingemar : " + n.second)

    def test_generate_swedish_female_last_name_svensson(self):
        """Generate a swedish female name with surname Svensson"""
        n=Name(gender="female", countrycode='sv', surname ="Svensson" )
        print("Surname : ", n.surname)
        self.assertEqual(n.surname,'Svensson', "The surname should be Svensson : " + n.surname)

    def test_generate_swedish_female_full_name(self):
        """Generate a swedish full female name with surname Dahl"""
        n=Name(gender="female", countrycode='sv', first='Astrid', second='Dollis', surname ="Dahlgren" )
        print("First : ", n.first, "Second : ", n.second, "  Surname: ", n.surname)
        self.assertEqual(n.first,'Astrid', "The first name should be Astrid : " + n.first)
        self.assertEqual(n.second,'Dollis', "The second name should be Dollis : " + n.second)
        self.assertEqual(n.surname,'Dahlgren', "The surname should be Dahlgren : " + n.surname)



    ### Finish names

    def test_generate_swedish_name(self):
        """Generate random finish name"""
        n=Name(countrycode='fi')
        print("Swedish random name : ", n)
        self.assertGreater(len(n.first),1, "This name is too short")


    def test_generate_finish_male_name(self):
        """Generate a finish male name"""
        n=Name(gender="male", countrycode='fi')
        print("Male Name : ", n.first)
        self.assertGreater(len(n.first),1, "This name is too short : " + n.first)

    def test_generate_finish_male_name_birger(self):
        """Generate a finish male name with first name Birger"""
        n=Name(gender="male", countrycode='fi', first="Birger" )
        print("Male Name : ", n.first)
        self.assertEqual(n.first,'Birger', "This name should be Birger : " + n.first)

    def test_generate_finish_male_second_name_ingemar(self):
        """Generate a finish male name with first name Ingemar"""
        n=Name(gender="male", countrycode='fi', second="Ingemar" )
        print("Male Name : ", n.second)
        self.assertEqual(n.second,'Ingemar', "The second name should be Ingemar : " + n.second)

    def test_generate_finish_male_last_name_svensson(self):
        """Generate a finish male name with surname Svensson"""
        n=Name(gender="male", countrycode='fi', surname ="Svensson" )
        print("Surname : ", n.surname)
        self.assertEqual(n.surname,'Svensson', "The surname should be Svensson : " + n.surname)

    def test_generate_finish_male_full_name(self):
        """Generate a finish full male name with surname Bergling"""
        n=Name(gender="male", countrycode='fi', first='Stig', second='Eugen', surname ="Bergling" )
        print("First : ", n.first, "Second : ", n.second, "  Surname: ", n.surname)
        self.assertEqual(n.first,'Stig', "The first name should be Stig : " + n.first)
        self.assertEqual(n.second,'Eugen', "The second name should be Eugen : " + n.second)
        self.assertEqual(n.surname,'Bergling', "The surname should be Bergling : " + n.surname)

    ### Finnish Females

    def test_generate_finish_female_name(self):
        """Generate a finish female name"""
        n=Name(gender="female", countrycode='fi')
        print("Female Name : ", n.first)
        self.assertGreater(len(n.first),1, "This name is too short : " + n.first)

    def test_generate_finish_female_name_birger(self):
        """Generate a finish female name with first name Arja"""
        n=Name(gender="female", countrycode='fi', first="Arja" )
        print("Female Name : ", n.first)
        self.assertEqual(n.first,'Arja', "This name should be Arja : " + n.first)

    def test_generate_finish_female_second_name_ingemar(self):
        """Generate a finish female name with secons name Enni"""
        n=Name(gender="female", countrycode='fi', second="Enni" )
        print("Female Name : ", n.second)
        self.assertEqual(n.second,'Enni', "The second name should be Enni : " + n.second)

    def test_generate_finish_female_last_name_svensson(self):
        """Generate a finish female name with surname Saijonmaa"""
        n=Name(gender="female", countrycode='fi', surname ="Saijonmaa" )
        print("Surname : ", n.surname)
        self.assertEqual(n.surname,'Saijonmaa', "The surname should be Saijonmaa : " + n.surname)

    def test_generate_finish_female_full_name(self):
        """Generate a finish full female name with surname Dahl"""
        n=Name(gender="female", countrycode='fi', first='Arja', second='Enni', surname ="Saijonmaa" )
        print("First : ", n.first, "Second : ", n.second, "  Surname: ", n.surname)
        self.assertEqual(n.first,'Arja', "The first name should be Arja : " + n.first)
        self.assertEqual(n.second,'Enni', "The second name should be Enni : " + n.second)
        self.assertEqual(n.surname,'Saijonmaa', "The surname should be Saijonmaa : " + n.surname)




