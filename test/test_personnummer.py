import unittest
import random
import sys
import time
import math
from personnummer.personnummer import Personnummer


class TestPersonnummer(unittest.TestCase):

    def setUp(cls):
        cls.seed = random.randrange(sys.maxsize)
        random.seed(cls.seed)
        print("Seeding using seed ",cls.seed)
      

    def test_generate_personnummer(self):
        p=Personnummer(year='1966',month=7,day=27,county=78,sex=1)
        self.assertEqual(p.number,'6607277818')

    def test_generate_personnummer_year_1916(self):
        p=Personnummer(year='1916')
        self.assertEqual(p.number[:2],'16')

    def test_generate_personnummer_month_01(self):
        p=Personnummer(month=1)
        self.assertEqual(p.number[2:4],'01')

    def test_generate_personnummer_month_12(self):
        p=Personnummer(month=12)
        self.assertEqual(p.number[2:4],'12')

    def test_generate_personnummer_day_01(self):
        p=Personnummer(day=1)
        self.assertEqual(p.number[4:6],'01')
   
    def test_generate_personnummer_day_28(self):
        p=Personnummer(day=28)
        self.assertEqual(p.number[4:6],'28')



