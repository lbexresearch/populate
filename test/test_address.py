import unittest
import random
import sys
import time
import math
from country.country import Country
from address.address import Address

def setUpModule():
    print("Setup Module")


class TestAddress(unittest.TestCase):
    """Test name generator"""
    @classmethod
    def setupClass(cls):
        cls.seed = random.randrange(sys.maxsize)
        random.seed(cls.seed)
        print("setuUp, using seed : ",cls.seed)

    def setup_module(self):
        print("Start Address Test")

    def setUp(self):
        """Setup test"""
        print("Test : ", self.id()) 

    def tearDown(self):
        """Tear down test"""
        print("Number of testcases and seed", self.countTestCases(), self.seed)

    def test_swedish_address(self):
        """Generate Address"""
        sweden=Country('sv')
        a=Address(country=sweden)
        self.assertGreater(len(a.street),4, "This street name is too short")
        self.assertLessEqual(len(a.street), 25)
        self.assertLessEqual(a.street_number, 99999)
        self.assertLessEqual(len(a.city.name), 25)
        print("OK : Random Swedish Address : ", a.street, a.street_number, ", ", a.city.name)
